package dts.com.br.exemplobroadcastnotificacao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


public class ConnectionBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Util.getInstance(context).createNotification();
        Toast.makeText(context, "chegou", Toast.LENGTH_LONG).show();
        Util.getInstance(context).notifyListener();
    }

}





