package br.com.dts.alarmmanager;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

public class AlarmReceiver extends BroadcastReceiver {

    private NotificationManager mNotificationManager;

    public static final int SIMPLE_NOTIFICATION_ID = 10001;

    @Override
    public void onReceive(Context context, Intent intent) {

        createSimpleNotification(context);

    }

    private void createSimpleNotification(Context c) {
        mNotificationManager =
                (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(c)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Notificação simples")
                        .setAutoCancel(false) //Não faça isso
                        .setContentText("Clique para saber mais...");


        Intent resultIntent = new Intent(c, MainActivity.class);




        TaskStackBuilder stackBuilder = TaskStackBuilder.create(c);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        mNotificationManager.notify(SIMPLE_NOTIFICATION_ID, mBuilder.build());
    }
}
