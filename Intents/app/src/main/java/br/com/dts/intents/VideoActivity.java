package br.com.dts.intents;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {

    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mVideoView = new VideoView(this);

        String videoPath = getIntent().getStringExtra("EXTRA_VIDEO_PATH");

        Uri myUri = Uri.parse(videoPath);

        //mVideoView.setVideoPath(videoPath);

        mVideoView.setVideoURI(myUri);


        mVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVideoView.start();
            }
        });

        setContentView(mVideoView);

    }

}
