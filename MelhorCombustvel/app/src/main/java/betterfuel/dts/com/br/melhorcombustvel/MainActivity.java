package betterfuel.dts.com.br.melhorcombustvel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText mEdtAlcool;
    private EditText mEdtGasolina;
    private Button mBtnCalcular;
    private static final double FATOR_DE_CALCULO = 0.7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEdtAlcool = (EditText)findViewById(R.id.edt_alcool);
        mEdtGasolina = (EditText)findViewById(R.id.edt_gasolina);

        mBtnCalcular = (Button) findViewById(R.id.btn_calcular);

        mBtnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double valorAlcool = Double.parseDouble(mEdtAlcool.getText().toString());
                    double valorGasolina = Double.parseDouble(mEdtGasolina.getText().toString());

                    String resultado = (useAlcool(valorAlcool, valorGasolina)) ?
                            getString(R.string.choice_alcool) : getString(R.string.choice_gas) ;

                    Toast.makeText(MainActivity.this, resultado, Toast.LENGTH_LONG).show();

                } catch (NumberFormatException ex){
                    Toast.makeText(MainActivity.this, getString(R.string.incorrect_values), Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public boolean useAlcool(double valorAlcool, double valorGasolina){

        return (valorAlcool <= valorGasolina * FATOR_DE_CALCULO ) ;

    }
}
