package t2i.com.br.fragmentsample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by wallace on 16/12/17.
 */

public class MyCustomAdapter extends BaseAdapter {

    private Context context;
    private List<Team> teams;

    /**
     * Construtor do adapter
     * @param context Contexto da aplicação.
     * @param teamList lista de conteudo que irá ser exibido na listView
     */
    public MyCustomAdapter(Context context, List<Team> teamList) {
        this.context = context;
        this.teams = teamList;
    }

    @Override
    public int getCount() {
        //Retorna o tamanho da lista que será carregada.
        return teams.size();
    }

    @Override
    public Object getItem(int position) {
        //Retorna o item na posição passada por parametro
        return teams.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        View itemView;
        //Recupera o item da lista na posição do listView.
        Team team = teams.get(position);

        //Checa se esse item da lista ja foi criada.
        if (view == null) {
            //Se o item ainda não foi criado inicializa o viewHolder
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);

            //Infla o layout que será carregado no listView
            itemView = inflater.inflate(R.layout.list_item, viewGroup, false);

            //Seta o id das view TextView e ImageView.
            holder.imgLogo = itemView.findViewById(R.id.imLogo);
            holder.teamName = itemView.findViewById(R.id.txtTeamName);

            //salva a tag no item para recuperar no scroll.
            itemView.setTag(holder);


        } else {
            //Recupera o item pela tag setada anteriormente.
            holder = (ViewHolder) view.getTag();
            //Associa a view ao item do listView
            itemView = view;
        }

        //Seta o nome do time e a logo do time.
        holder.teamName.setText(team.getName());
        holder.imgLogo.setImageResource(team.getImageID());

        return itemView;
    }

    /**
     * Classe que será reutilizada nos itens do listView
     */
    private static class ViewHolder {
        ImageView imgLogo;
        TextView teamName;
    }
}
