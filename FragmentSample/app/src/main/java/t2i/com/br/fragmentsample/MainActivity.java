package t2i.com.br.fragmentsample;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Cria a instancia do fragment
        MyFragment fragment = MyFragment.newInstance();

        //Recupera a instancia do fragment manager responsável  por gerenciar os fragments.
        FragmentManager manager = getFragmentManager();
        //Inicia uma transação para adicionar, remover ou alterar o fragment.
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_place, fragment);
        //Realiza a alteração. Importante sem o commando abaixo o fragment não é atualizado.
        transaction.commit();
    }
}
