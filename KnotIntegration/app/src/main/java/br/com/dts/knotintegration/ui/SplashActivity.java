package br.com.dts.knotintegration.ui;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import br.com.dts.knotintegration.R;
import br.com.dts.knotintegration.util.PreferenceUtil;


public class SplashActivity extends AppCompatActivity {

    private static final int TIME_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String UUID = PreferenceUtil.getInstance().getUuid();
                String token = PreferenceUtil.getInstance().getTonken();
                String URL = PreferenceUtil.getInstance().getEndPoint();

                if (!UUID.equals("") && !token.equals("") && !URL.equals("")){
                    startActivity(new Intent(SplashActivity.this, GatewaysActivity.class));
                } else{
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }

                //startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }, TIME_DELAY);
    }
}
