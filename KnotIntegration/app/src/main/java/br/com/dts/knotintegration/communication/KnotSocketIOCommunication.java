/*
 * Copyright (c) 2017, CESAR.
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *
 *
 */

package br.com.dts.knotintegration.communication;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.List;

import br.com.dts.knotintegration.model.SwitchData;
import br.com.dts.knotintegration.model.SwitchDevice;
import br.com.dts.knotintegration.util.Logger;
import br.com.dts.knotintegration.util.PreferenceUtil;
import br.com.dts.knotintegration.util.Util;
import br.org.cesar.knot.lib.connection.FacadeConnection;
import br.org.cesar.knot.lib.event.Event;
import br.org.cesar.knot.lib.exception.InvalidParametersException;
import br.org.cesar.knot.lib.exception.KnotException;
import br.org.cesar.knot.lib.exception.SocketNotConnected;
import br.org.cesar.knot.lib.model.KnotList;
import br.org.cesar.knot.lib.model.KnotQueryData;
import br.org.cesar.knot.lib.model.KnotQueryDateData;
import br.org.cesar.knot.lib.util.DateUtils;
import br.org.cesar.knot.lib.util.LogLib;



public class KnotSocketIOCommunication implements KnotCommunication {

    private static final String ENDPOINT = Util.KNOT_URL;
    private static final String UUID_OWNER = Util.DEFAULT_UUID;
    private static final String TOKEN_OWNER = Util.DEFAULT_TOKEN;

    private static final String OWNER = "owner";

    private static final Object lock = new Object();

    /**
     * Class used to access the db repository
     */
    //private FacadeDatabase mDrinkFountainDB;

    /**
     * Class used to access knot LIB
     */
    public FacadeConnection mKnotApi;

    private List<SwitchDevice> mSwitchDeviceList;

    private List<SwitchData> mSwitchDataList;

    /**
     * Only Instance
     */
    private static KnotSocketIOCommunication sInstance;


    /**
     * Private constructor
     */
    private KnotSocketIOCommunication() {
        //Initializing the KNOT API
        mKnotApi = FacadeConnection.getInstance();
        mKnotApi.setupSocketIO(UUID_OWNER, TOKEN_OWNER);
        // Configuring the API


        //Initializing the DATABASE to save app information
        //mDrinkFountainDB = FacadeDatabase.getInstance();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static KnotSocketIOCommunication getInstance() {
        synchronized (lock) {
            if (sInstance == null) {
                sInstance = new KnotSocketIOCommunication();
            }
            return sInstance;
        }
    }

    /**
     * Authenticating the socket communication
     *
     * @param callbackResult Callback that will receive the result
     */
    public void authenticateSocketCommunication(final Event<Boolean> callbackResult) {

        try {
            mKnotApi.connectSocket(ENDPOINT, new Event<Boolean>() {
                @Override
                public void onEventFinish(Boolean object) {
                    try {
                        mKnotApi.socketIOAuthenticateDevice(callbackResult);
                    } catch (SocketNotConnected socketNotConnected) {
                       // LogKnotDrinkFountain.printE(socketNotConnected);
                    } catch (InvalidParametersException e) {
                       // LogKnotDrinkFountain.printE(e);
                    }
                }

                @Override
                public void onEventError(Exception e) {
                  //  LogKnotDrinkFountain.printE(e);
                    callbackResult.onEventError(e);
                }
            });
        } catch (SocketNotConnected socketNotConnected) {
            socketNotConnected.printStackTrace();
            callbackResult.onEventError(socketNotConnected);
        }

    }

    @Override
    public void getAllDevices() {
        KnotList<SwitchDevice> mDrinkFountainDeviceList = new KnotList<>(SwitchDevice.class);

        JSONArray ja = new JSONArray();
        //ID do gateway, * to all
        ja.put("*");

        JSONObject query = new JSONObject();
        try {
            query.put("gateways", ja);
        } catch (JSONException e) {
            //LogKnotDrinkFountain.printE(e);
        }

        try {
            mKnotApi.socketIOGetDeviceList(mDrinkFountainDeviceList, query, new Event<List<SwitchDevice>>() {

                @Override
                public void onEventFinish(List<SwitchDevice> deviceList) {
                    Logger.d("lista de devices size = " + deviceList.size());
                    if (deviceList != null) {
                        mSwitchDeviceList = deviceList;
                        for (SwitchDevice d : deviceList){
                           // Log.d("Diego", "Desc = " +  d.getUuid());
                            Logger.d("=============DEVICE===============");
                            Logger.d("UUID = " + d.getUuid());
                            //getDataByDevice();
                        }
                    }

                    //mDrinkFountainDB.insertDrinkFountainList(deviceList);


                    //KnotHttpCommunication.getInstance().getDataByDevice();
                }

                @Override
                public void onEventError(Exception e) {
                    //LogKnotDrinkFountain.printE(e);
                    //KnotHttpCommunication.getInstance().getDataByDevice();
                }
            });
        } catch (KnotException e) {
            //LogKnotDrinkFountain.printE(e);
            e.printStackTrace();
        } catch (SocketNotConnected socketNotConnected) {
           // LogKnotDrinkFountain.printE(socketNotConnected);
            socketNotConnected.printStackTrace();
        } catch (InvalidParametersException e) {
           // LogKnotDrinkFountain.printE(e);
            e.printStackTrace();
        }
    }

    @Override
    public void getDataByDevice() {

        KnotList<SwitchData> switchDataList = new KnotList<>(SwitchData.class);

        for (final SwitchDevice drinkFountainDevice : mSwitchDeviceList) {

            // get the last valid waterLevelData to build the query
            //SwitchData waterLevelData = mSwitchDataList.get(0);

            KnotQueryDateData knotQueryDateDataStart = null;
            KnotQueryDateData knotQueryDateDataFinish = null;


            //Verify if the waterLevelData is valid
//            if (waterLevelData != null) {
//                String timeStamp = Util.convertMillisecondsToValidFormat(Long.parseLong(waterLevelData.getTimestamp()));
//
//                try {
//                    //Building the start date
//                    knotQueryDateDataStart = DateUtils.getKnotQueryDateData(timeStamp);
//                } catch (ParseException e) {
//                    //LogKnotDrinkFountain.printE(e);
//                }
//
//            }

            try {
                //get the current hour of the system
                knotQueryDateDataFinish = DateUtils.getCurrentKnotQueryDateData();

                KnotQueryData knotQueryData = new KnotQueryData();
                knotQueryData.setFinishDate(knotQueryDateDataFinish);
                //        setStartDate(knotQueryDateDataStart
                       // );

                mKnotApi.socketIOGetData(switchDataList, UUID_OWNER, TOKEN_OWNER, null, new Event<List<SwitchData>>() {
                    @Override
                    public void onEventFinish(List<SwitchData> list) {
                        try {
                            mSwitchDataList = list;

                            if (mSwitchDataList != null) {
                                for (SwitchData data : mSwitchDataList){
                                    Logger.d("==============DATA===================");
                                    Logger.d(Boolean.toString(data.getCurrentValue()));
                                }
                            }
                        } catch (Exception e) {
                            Logger.d("==============EXCEPTION===================");
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onEventError(Exception e) {
                        //LogKnotDrinkFountain.printE(e);
                    }
                });
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (InvalidParametersException e) {
               e.printStackTrace();
            } catch (SocketNotConnected socketNotConnected) {
                socketNotConnected.printStackTrace();
            }


        }


    }

}
