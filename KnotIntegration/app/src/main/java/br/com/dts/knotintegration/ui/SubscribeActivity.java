package br.com.dts.knotintegration.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.IBinder;

import android.os.Bundle;

import android.widget.ImageView;

import java.util.List;

import br.com.dts.knotintegration.model.SwitchData;
import br.com.dts.knotintegration.service.IKnotServiceConnection;
import br.com.dts.knotintegration.service.KnotIntegrationService;
import br.com.dts.knotintegration.service.OnDataChangedListener;
import br.com.dts.knotintegration.service.ServiceBinder;
import br.com.dts.knotintegration.util.Util;

public class SubscribeActivity extends Activity implements OnDataChangedListener, ServiceConnection {

    private IKnotServiceConnection mKnotServiceConnection;

    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mImageView = new ImageView(this);

        setContentView(mImageView);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent it = new Intent(this, KnotIntegrationService.class);
        startService(it);
        bindService(it, this, BIND_AUTO_CREATE);
    }


    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
    }

    @Override
    public void onDataChanged(List<SwitchData> deviceData) {
        //Toast.makeText(this, "Data = " + deviceData.toString(), Toast.LENGTH_LONG).show();
        String text = null;
        for (SwitchData data: deviceData){
            if (data != null && data.getCurrentValue()){
               mImageView.setBackgroundColor(Color.GREEN);
            }else {
                mImageView.setBackgroundColor(Color.RED);
            }
        }


    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mKnotServiceConnection = ((ServiceBinder) service).getService();

        mKnotServiceConnection.subscribe(getIntent().getStringExtra(Util.EXTRA_DEVICE_UUID), SubscribeActivity.this);

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mKnotServiceConnection = null;
    }
}
