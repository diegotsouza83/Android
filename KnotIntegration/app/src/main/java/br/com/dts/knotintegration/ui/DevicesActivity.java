package br.com.dts.knotintegration.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.dts.knotintegration.R;
import br.com.dts.knotintegration.model.SwitchData;
import br.com.dts.knotintegration.ui.adapter.SwitchDataAdapter;
import br.com.dts.knotintegration.util.Logger;
import br.com.dts.knotintegration.util.Util;
import br.org.cesar.knot.lib.connection.FacadeConnection;
import br.org.cesar.knot.lib.exception.InvalidDeviceOwnerStateException;
import br.org.cesar.knot.lib.exception.KnotException;
import br.org.cesar.knot.lib.model.KnotList;
import br.org.cesar.knot.lib.model.KnotQueryData;

public class DevicesActivity extends AppCompatActivity {

    private String UUID;
    private List<SwitchData> mDeviceData;

    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UUID = getIntent().getStringExtra(Util.EXTRA_DEVICE_UUID);

        if (UUID != null && !UUID.isEmpty()){

            new SyncDeviceDataTask().execute();

        } else {
            Toast.makeText(this, R.string.invalid_UUID, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.menu_subscribe, 0, 0, R.string.menu_subscribe);
        menu.add(R.string.menu_send_data, 1, 1, R.string.menu_send_data);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case 0:
                Intent intent = new Intent(this, SubscribeActivity.class);
                intent.putExtra(Util.EXTRA_DEVICE_UUID, UUID);
                startActivity(intent);
                break;
            case 1:
                Intent it = new Intent(this, SendCommandActivity.class);
                it.putExtra(Util.EXTRA_DEVICE_UUID, UUID);
                //For defaul, consider only the first device. Should be changed if it has others in the list
                it.putExtra(Util.EXTRA_CURRENT_VALUE,mDeviceData.get(0).getCurrentValue());
                startActivity(it);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SyncDeviceDataTask extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {

            KnotList<SwitchData> listOfData = new KnotList<>(SwitchData.class);
            KnotQueryData knotQueryData = new KnotQueryData();

            List<SwitchData> deviceList = new ArrayList<>();


            try {

                Logger.d("UUID = " + UUID);
                mDeviceData = FacadeConnection.getInstance().httpGetDataList(UUID, null,listOfData);

                for(SwitchData data : mDeviceData){
                    if (data != null && !contais(deviceList, data)){
                        deviceList.add(data);
                    }
                }

                Logger.d("Size = " + mDeviceData.size());

                mDeviceData = deviceList;

                for (SwitchData data : mDeviceData){
                    Logger.d("value = " + data.getCurrentValue());
                    Logger.d("sensor = " + data.data.sensor_id);
                }

            } catch (KnotException e) {
                e.printStackTrace();
                Logger.d("KnotExecption");
            } catch (InvalidDeviceOwnerStateException e) {
                e.printStackTrace();
                Logger.d("InvalidDeviceOwnerStateException");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if (mDeviceData != null && mDeviceData.size() > 0) {
                for (SwitchData data : mDeviceData){
                    Logger.d(data.toString());

                }
                Toast.makeText(DevicesActivity.this, "List size = " + mDeviceData.size(), Toast.LENGTH_LONG).show();
                initListView();
            }
        }
    }

    private void initListView(){
        mListView = new ListView(this);

        mListView.setAdapter(new SwitchDataAdapter(this, mDeviceData));

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SwitchData deviceData = (SwitchData) parent.getItemAtPosition(position);

                if (deviceData != null) {
                    Toast.makeText(DevicesActivity.this, deviceData.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });

        setContentView(mListView);

    }

    private boolean contais(List<SwitchData> list, SwitchData data){
        for (SwitchData dataLits : list){
            if(dataLits != null && dataLits.data.sensor_id == data.data.sensor_id){
                return true;
            }
        }

        return false;

    }
}
