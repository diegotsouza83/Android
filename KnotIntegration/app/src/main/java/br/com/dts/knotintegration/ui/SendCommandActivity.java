package br.com.dts.knotintegration.ui;

import android.media.FaceDetector;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.widget.ToggleButton;


import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.dts.knotintegration.R;
import br.com.dts.knotintegration.model.SwitchDataMessage;
import br.com.dts.knotintegration.model.SwitchDevice;
import br.com.dts.knotintegration.util.Logger;
import br.com.dts.knotintegration.util.Util;
import br.org.cesar.knot.lib.connection.FacadeConnection;
import br.org.cesar.knot.lib.exception.InvalidDeviceOwnerStateException;
import br.org.cesar.knot.lib.exception.InvalidParametersException;
import br.org.cesar.knot.lib.exception.KnotException;
import br.org.cesar.knot.lib.exception.SocketNotConnected;
import br.org.cesar.knot.lib.model.KnotList;

public class SendCommandActivity extends AppCompatActivity {

    private ToggleButton mToggleButton;

    private boolean mCurrentStatus;

    private Handler mHandler = new Handler() {
        @Override
        public String getMessageName(Message message) {

            mToggleButton.setChecked(mCurrentStatus);
            return super.getMessageName(message);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_command);


        mCurrentStatus = getIntent().getBooleanExtra(Util.EXTRA_CURRENT_VALUE, false);

        mToggleButton = (ToggleButton)findViewById(R.id.toogle_cmd);

        mToggleButton.setChecked(mCurrentStatus);

        mToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatus();
            }
        });

    }

    //@WorkerThread
    private void sendCommand(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                SwitchDataMessage switchMessage = new SwitchDataMessage();

                // create the second device
                SwitchDevice device = new SwitchDevice();

                switchMessage.getDevices().add(getIntent().getStringExtra(Util.EXTRA_DEVICE_UUID));

                switchMessage.setMessage(Boolean.toString(mCurrentStatus));

                try {
                     //Send your data here

                    //Create and send message to handler due to use the main thread
                    Message message = new Message();
                    //message.obj = null;

                    mHandler.sendMessage(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void setStatus(){

        mCurrentStatus = !mCurrentStatus;

        sendCommand();
    }


}
