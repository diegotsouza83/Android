
package br.com.dts.knotintegration.util;

import android.content.Context;
import android.net.ConnectivityManager;

import java.net.InetAddress;

import java.util.Calendar;


import br.com.dts.knotintegration.application.App;




public final class Util {

    public static final String EMPTY_STRING = "";
    private static final String URL_TO_PING = "google.com";
    public static final String EXTRA_DEVICE_UUID = "DEVICE_UUID";
    public static final String EXTRA_CURRENT_VALUE = "CURRENT_VALUE";
    public static final String KNOT_URL = "http://knot-test.cesar.org.br:3000";

    //public static final String DEFAULT_UUID = "6e455b0d-3be1-4052-8ba4-99b270220000";
    //public static final String DEFAULT_TOKEN = "8449949da1e33c0949e53210bab6696eb9d9a4d3";

    public static final String DEFAULT_UUID = "a9cc3e68-abea-4541-972a-39cf32bd0000";
    public static final String DEFAULT_TOKEN = "097ee5c09e978c3ebc3841c362107de832d50664";
    private Util(){

    }

    public static int getDateByTime(String time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(time));

        int month = calendar.get(Calendar.MONTH);
        int day   = calendar.get(Calendar.DAY_OF_MONTH);

        return Integer.parseInt(month+""+day);
    }

    public static boolean hasIntenetConnection(){
        return hasConnection() && isInternetAvailable();
    }

    protected static boolean hasConnection(){
        ConnectivityManager cm = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return (cm.getActiveNetworkInfo() != null);
    }

    protected static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName(URL_TO_PING);
            return !ipAddr.equals(EMPTY_STRING);

        } catch (Exception e) {
            //Something wrong on ping
            return false;
        }
    }

}
